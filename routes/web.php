<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index')->name('index');
Route::get('/getConsumptionData', 'Controller@getConsumptionData')->name('getConsumptionData');
Route::get('/getdailyConsumptionData', 'Controller@getdailyConsumptionData')->name('getdailyConsumptionData');
Route::get('/getMonthlyConsumptionData', 'Controller@getMonthlyConsumptionData')->name('getMonthlyConsumptionData');
