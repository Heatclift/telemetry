<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $data_id
 * @property string $date
 * @property float $load
 * @property float $level
 * @property float $trend
 * @property float $seasonal
 * @property float $forecast
 * @property float $error
 */
class data_table extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'data_table';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'data_id';

    /**
     * @var array
     */
    protected $fillable = ['date', 'load', 'level', 'trend', 'seasonal', 'forecast', 'error'];

}
