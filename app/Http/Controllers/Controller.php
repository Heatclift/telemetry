<?php

namespace App\Http\Controllers;

use App\Charts\solidGuageChart;
use App\Charts\powerChart;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\data_table;
use Carbon\Carbon;

class Controller extends BaseController
{
    use  DispatchesJobs, ValidatesRequests;

    public function index(Request $req)
    {
        $avgErrRate = data_table::all()->avg('error');

        $avgErrRate = number_format((float)abs($avgErrRate), 2, '.', '');


        $arr["PowerChart"] = new powerChart;
        $arr["PowerChart"]->title('Hourly Power Cunsumption');
        $arr["PowerChart"]->label('Power (kW)');
        $arr["PowerChart"]->options([
            'tooltip' => [
                'crosshairs' => true,
                'shared' => true
            ],
            'xAxis' => [
                'show' => true,
                'type' => 'datetime',
            ]
        ]);
        $arr["PowerChart"]->dataset('Load', 'line', [])->options([
            'color' => '#FF0000'
        ]);
        $arr["PowerChart"]->dataset('Power Level', 'line', [])->options([
            'color' => '#00ff00'
        ]);

        $arr["dailyPowerChart"] = new powerChart;
        $arr["dailyPowerChart"]->title('Daily Power Cunsumption');
        $arr["dailyPowerChart"]->label('Power (kW)');
        $arr["dailyPowerChart"]->options([
            'tooltip' => [
                'crosshairs' => true,
                'shared' => true
            ],
            'xAxis' => [
                'show' => true,
                'type' => 'datetime',
            ]
        ]);
        $arr["dailyPowerChart"]->dataset('Load', 'line', [])->options([
            'color' => '#FF0000'
        ]);
        $arr["dailyPowerChart"]->dataset('Power Level', 'line', [])->options([
            'color' => '#00ff00'
        ]);
        $arr["dailyPowerChart"]->dataset('Forcast (avg. err Rate:+- ' . $avgErrRate . ')', 'line', [])->options([
            'color' => '#a759cf'
        ]);

        $arr["monthlyPowerChart"] = new powerChart;
        $arr["monthlyPowerChart"]->title('Monthly Power Cunsumption');
        $arr["monthlyPowerChart"]->label('Power (kW)');
        $arr["monthlyPowerChart"]->options([
            'tooltip' => [
                'crosshairs' => true,
                'shared' => true
            ],
            'xAxis' => [
                'show' => true,
                'type' => 'datetime',
            ]
        ]);
        $arr["monthlyPowerChart"]->dataset('Load', 'line', [])->options([
            'color' => '#FF0000'
        ]);
        $arr["monthlyPowerChart"]->dataset('Power Level', 'line', [])->options([
            'color' => '#00ff00'
        ]);
        $arr["monthlyPowerChart"]->dataset('Forcast (avg. err Rate:+- ' . $avgErrRate . ')', 'line', [])->options([
            'color' => '#a759cf'
        ]);


        $this->getConsumptionData();
        return view('index')->with($arr);
    }

    public function getConsumptionData()
    {
        $consumptionData = data_table::whereDate('date', '>=', Carbon::today()->subDays(1)->toDateString())->orderBy('date', 'ASC')->get()->groupBy(function ($date) {
            return Carbon::parse($date->date)->format('Ymdhm');
        });
        $processedData = $consumptionData->map(function ($item, $key) {
            $jsonDateTime = Carbon::parse('1970-01-01')->diffInSeconds(Carbon::parse($item->first()->date)->toDayDateTimeString()) * 1000;
            return
                ["dateTime" => $jsonDateTime, "level" => $item->sum("level"), "load" => $item->sum("load"), "forecast" => $item->sum("forecast")];
        });

        return response()->json($processedData);
    }

    public function getMonthlyConsumptionData()
    {
        $consumptionData = data_table::whereDate('date', '>=', Carbon::today()->subMonths(24)->toDateString())->orderBy('date', 'ASC')->get()->groupBy(function ($date) {

            return Carbon::parse($date->date)->format('Ym');
        });
        $processedData = $consumptionData->map(function ($item, $key) {
            $jsonDateTime = Carbon::parse('1970-01-01')->diffInSeconds(Carbon::parse($item->first()->date)->format('Y-m')) * 1000;
            return
                ["dateTime" => $jsonDateTime, "level" => $item->sum("level"), "load" => $item->sum("load"), "forecast" => $item->sum("forecast")];
        });

        return response()->json($processedData);
    }

    public function getdailyConsumptionData()
    {
        $consumptionData = data_table::whereDate('date', '>=', Carbon::today()->subMonths(24)->toDateString())->orderBy('date', 'ASC')->get()->groupBy(function ($date) {

            return Carbon::parse($date->date)->format('Ymd');
        });
        $processedData = $consumptionData->map(function ($item, $key) {
            $jsonDateTime = Carbon::parse('1970-01-01')->diffInSeconds(Carbon::parse($item->first()->date)->format('Y-m-d')) * 1000;
            return
                ["dateTime" => $jsonDateTime, "level" => $item->sum("level"), "load" => $item->sum("load"), "forecast" => $item->sum("forecast")];
        });

        return response()->json($processedData);
    }
}
