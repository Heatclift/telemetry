@extends('layout.administrator')

@section('content')
    {!! $PowerChart->container() !!}

    {!! $dailyPowerChart->container() !!}

    {!! $monthlyPowerChart->container() !!}
@endsection


@section('scripts')
    <script src="{{ asset('highcartsJS/highcharts.js') }}"></script>
    <script src="{{ asset('highcartsJS/data.js') }}"></script>
    <script src="{{ asset('highcartsJS/exporting.js') }}"></script>
    <script src="{{ asset('highcartsJS/offline-exporting.js') }}"></script>
    <script src="{{ asset('highcartsJS/export-data.js') }}"></script>
    <script src="{{ asset('highcartsJS/accessibility.js') }}"></script>

    {!! $PowerChart->script() !!}
    {!! $dailyPowerChart->script() !!}
    {!! $monthlyPowerChart->script() !!}

    <script>
        var filterAPI = "{{ route('getConsumptionData') }}";
        var dailyfilterAPI = "{{ route('getdailyConsumptionData') }}";
        var monthlyfilterAPI = "{{ route('getMonthlyConsumptionData') }}";
        $(document).ready(function() {

            ////refreshes the live chart
            var updatePowerConsumptionChart = function() {
                $.ajax({
                    url: filterAPI,
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        var series = {{ $PowerChart->id }}.series[0],
                            shift = series.data.length > 20;
                        var dataArray = Object.keys(data).map(function(k) {
                            return data[k]
                        });
                        var powerlvl = dataArray.map(item => [item.dateTime, (item.level != 0 &&
                                item.level != null) ?
                            parseFloat(item.level) : null
                        ]);
                        var powerload = dataArray.map(item => [item.dateTime, (item.load != 0 &&
                                item.load != null) ?
                            parseFloat(item.load) : null
                        ]);


                        {{ $PowerChart->id }}.series[0].setData(powerload);
                        {{ $PowerChart->id }}.series[1].setData(powerlvl);
                        setTimeout(updatePowerConsumptionChart, 1000);

                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
                $.ajax({
                    url: dailyfilterAPI,
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        var series = {{ $dailyPowerChart->id }}.series[0],
                            shift = series.data.length > 20;
                        var dataArray = Object.keys(data).map(function(k) {
                            return data[k]
                        });
                        var powerlvl = dataArray.map(item => [item.dateTime, (item.level != 0 &&
                                item.level != null) ?
                            parseFloat(item.level) : null
                        ]);

                        var powerload = dataArray.map(item => [item.dateTime, (item.load != 0 &&
                                item.load != null) ?
                            parseFloat(item.load) : null
                        ]);

                        var forecast = dataArray.map(item => [item.dateTime, (item.forecast != 0 &&
                                item.forecast != null) ?
                            parseFloat(item.forecast) : null
                        ]);


                        {{ $dailyPowerChart->id }}.series[0].setData(powerload);
                        {{ $dailyPowerChart->id }}.series[1].setData(powerlvl);
                        {{ $dailyPowerChart->id }}.series[2].setData(forecast);


                    },
                    error: function(data) {
                        console.log(data);
                    }

                });
                $.ajax({
                    url: monthlyfilterAPI,
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        var series = {{ $monthlyPowerChart->id }}.series[0],
                            shift = series.data.length > 20;
                        var dataArray = Object.keys(data).map(function(k) {
                            return data[k]
                        });
                        var powerlvl = dataArray.map(item => [item.dateTime, (item.level != 0 &&
                                item.level != null) ?
                            parseFloat(item.level) : null
                        ]);

                        var powerload = dataArray.map(item => [item.dateTime, (item.load != 0 &&
                                item.load != null) ?
                            parseFloat(item.load) : null
                        ]);

                        var forecast = dataArray.map(item => [item.dateTime, (item.forecast != 0 &&
                                item.forecast != null) ?
                            parseFloat(item.forecast) : null
                        ]);


                        {{ $monthlyPowerChart->id }}.series[0].setData(powerload);
                        {{ $monthlyPowerChart->id }}.series[1].setData(powerlvl);
                        {{ $monthlyPowerChart->id }}.series[2].setData(forecast);


                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }

            updatePowerConsumptionChart();

        });

    </script>

@endsection
