<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aims to provide the need of everyone in terms of hiring skilled workers and provide alternative employment to TESDA certified job-seekers.">
    <meta name="author" content="#TeamAVODAH">
    <title>Avodah - Serving with Integrity </title>
  
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i|IBM+Plex+Sans+Condensed:400,400i|IBM+Plex+Sans:100,100i,400,400i,700,700i|IBM+Plex+Serif:400,400i&display=swap" rel="stylesheet">
     <!--Icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Theme CSS --> 
    <link type="text/css" href="{{ asset('assets/css/theme.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />
    <link rel="" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
    <link rel="" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('custom/customcss.css') }}" /> 
    <link rel="stylesheet" href="{{ asset('custom/customAnimations.css') }}" /> 
     @yield('css')
    
   
</head>
{{--  loader  --}}

<div class="loader">
    <img id="heart" class ="img-responsive block-image animated infinite heartBeat" src="{{ asset('assets/images/avodah-logo_heart.png') }}" style="with:auto; height:7%;" alt="">
    
</div>

{{--  end loader  --}}
<body>

    <nav class="navbar navbar-expand-sm navbar-light bg-white py-2" style=" box-shadow: 0 3px 5px -2px rgba(0,0,0,.2);">
      <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}"><img class ="img-responsive block-image" src="{{ asset('assets/images/avodah-logo.png') }}" width="250" height="auto" alt=""></a>
        <button class="navbar-toggler" type="button" data-action="offcanvas-open" data-target="#navbar_main" aria-controls="navbar_main" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse offcanvas-collapse nav-dark" id="navbar_main">
          <ul class="navbar-nav ml-auto align-items-lg-center">
            <h6 class="dropdown-header font-weight-600 d-lg-none px-0">Menu</h6>
            <li class="nav-item  @yield('index')">
              <a class="nav-link" href="{{ route('index') }}">Home</a>
            </li>
            <li class="nav-item dropdown @yield('dropdown')">
              <a class="nav-link dropdown-toggle " href="#" id="navbar_main_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
              <div class="dropdown-menu" aria-labelledby="navbar_1_dropdown_1">
                  <a class="dropdown-item" href="{{ route('about') }}">What is Avodah?</a>
                  <a class="dropdown-item" href="{{ route('faq') }}">FAQs</a>
                  <a class="dropdown-item" href="{{ route('contact') }}">Contact Us</a>
                  <a class="dropdown-item" href="{{ route('privacyPolicyWeb') }}">Privacy policy</a>
                  <a class="dropdown-item" href="{{ route('termsAndConditionsWeb') }}">Terms and Conditions</a>
              </div>
            </li>
            <li class="nav-item @yield('login')">
                <a class="nav-link" href="{{ route('signin') }}">Login</a>
            </li>
            <li class="nav-item @yield('registration')">
                <a class="nav-link" href="{{ route('registrationProviders') }}">Join us</a>
            </li>
            <li class="nav-item dropdown @yield('appdl')">
            <button type="button" id="btnservices" class="btn btn-primary bbtn-label btn-sm">Download app</button>
              
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div  id="app">
            @yield('content')
    </div>

    <footer class="pt-5 pb-3 footer footer-dark bg-tertiary">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 mb-3">
                    <h5 style="-webkit-text-fill-color: white; font-family: Montserrat, sans-serif; font-size: 30px">AVODAH</h5>
                    <p>Aims to provide the need of everyone in terms of hiring skilled workers and provide alternative employment to TESDA certified job-seekers.<br /></p>
                    
                    @yield('platforms', View::make('layouts.platforms'))
              </div>
                <div class="col-sm-6 col-md">
                    <h5 style="-webkit-text-fill-color: white">Contact Us</h5>
                    <ul class="list-unstyled text-small">
                        <li><strong>Address :</strong> AR-6 E-Nable One Bldg. Mabuhay I.T. Park, Ced Ave., National Highway, General Santos City 9500<br /></span></li>
                        <li><strong>Email :</strong> info@avodah.ph<br />
                        <li><strong>Office Number:</strong> (083) 877 3037<br /></li>
                    </ul>
            </div>
            <div class="col-2 col-md ">
                <h5 class="heading h6 text-uppercase font-weight-700 mb-3">About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ route('about') }}">What is Avodah?</a></li>
                   
                    <li><a class="text-muted" href="{{ route('faq') }}">FAQs</a></li>
                    <li><a class="text-muted" href="{{ route('contact') }}">Contact Us</a></li>
                  </ul>
              </div>
            </div>
            <hr>
          <div class="d-flex align-items-center">
            <span class="">
              &copy; 2019 <a href="https://www.facebook.com/Avodah-Philippines-456120434949487/" class="footer-link" target="_blank">#TeamAVODAH</a>. All rights reserved.
            </span>
            <ul class="nav ml-lg-auto">
                <li class="nav-item">
                    <a class="nav-link" href="https://www.facebook.com/Avodah-Philippines-456120434949487/" target="_blank"><i class="fab fa-facebook"></i></a>
                  </li>
         
            </ul>
          </div>
        </div>
      </footer>
      
  <!-- Core -->

     {{--  ///jquery  --}}
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    
     <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
       
        {{--  ///end jquery  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


    <script src="{{ asset('assets/vendor/fontawesome/js/fontawesome-all.min.js') }}" defer></script>
    <!-- Page plugins -->
    <script src="{{ asset('assets/vendor/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/input-mask/input-mask.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/nouislider/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/textarea-autosize/textarea-autosize.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/adaptive-backgrounds/adaptive-backgrounds.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/js/swiper.min.js') }}"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>
    <!-- Theme JS -->
    <script src="{{ asset('assets/js/theme.js') }}"></script>
     <script src="{{ asset('assets/js/script.min.js') }}"></script>
    
     <!--custom js-->

    <!--e show nya ang unod sa msg na variable -->
  <div class="modal fade" id="msgbox" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Message</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p id="msg"></p>
                  <div class="progress-wrapper">
                    <div class="progress" style="height: 20px; visibility: hidden;">
                       <div class="progress-bar bg-info" id ="pbar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  
                </div>
                <div class="modal-footer">

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
      
    @if(!@empty($msg))
    
    
          <script type="text/javascript">
            $(document).ready(function(){
                $("#msgbox").modal('show');
                  $("#msg").html("{{ $msg }}")
            });
        </script>
    @endif


@if(Session::has('message'))

          <script type="text/javascript">
            $(document).ready(function(){
                $("#msgbox").modal('show');
                $("#msg").html("{{ Session::get('message') }}")
            });
        </script>
    @endif

    <script>
    
        document.getElementById("btnservices").onclick = function() {
            location.href = "downloadApp";
        };

    </script>
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('custom/customAnimations.js') }}"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('custom/customjs.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.1.0/dist/lazyload.min.js"></script>
    <script>
      const myLazyLoad = new LazyLoad({
        elements_selector:".img-fluid"
      });
    </script>

    
    @yield('scripts')
    <!--end custom js-->
</body>
</html>
